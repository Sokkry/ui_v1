import React from 'react';
import { Row, Col } from 'react-bootstrap'

export default function Header() {
    return (
        <Row className="mt-4">
            <Col>
                <div className="first_page_logo">
                    <img src="./images/logo.png" width="142px" alt="" />
                </div>
            </Col>
            <Col>
                <div className="first_page_sch_logo">
                    <img src="./images/logo shcool.png" width="50px" alt="" />
                </div>
            </Col>
        </Row>
    )
}
