import React from 'react';
import { Container, Row, Col } from 'react-bootstrap'
import { Link, useHistory } from 'react-router-dom';
import Header from './Header';

export default function FirstPage() {

    const history = useHistory()


    //move to student login page
    const stuLogin = () => {
        history.push("/slogin")
    }


    //move to teacher login page
    const teachLogin = () => {
        history.push("/tlogin")
    }


    return (
        <Container>
            <div className="login_container">
                <Header />
                <Row className="mt-4">
                    <div className="card_role">
                        <Row>
                            <Col>
                                <h2 className="text-center text-white mt-4 mb-4">Pick your role</h2>
                            </Col>
                        </Row>
                        <Row>

                            <Col sm="6" xs="12">
                                <Link to="/sudentlogin" className="link_role_teacher">
                                    <div className="role_student">
                                        <img src="./images/profile.png" width="100%" alt="" />
                                        <p className="text-center mt-3 ">I am a student</p>
                                    </div>
                                </Link>
                            </Col>


                            <Col sm="6" xs="12">
                                <Link to="/teacherlogin" className="link_role_teacher">
                                    <div className="role_teacher">
                                        <img src="./images/profile.png" width="100%" alt="" />
                                        <p className="text-center mt-3 text-dark">I am a teacher</p>
                                    </div>
                                </Link>
                            </Col>

                            
                        </Row>
                    </div>
                </Row>
            </div>
        </Container>
    )
}
