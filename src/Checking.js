import React from 'react'
import { useEffect } from 'react'
import { useHistory } from 'react-router-dom'



export default function Checking({ islogin, isStudent }) {

    const history = useHistory()

    //យើងប្រើវាសម្រាប់  auto log ទៅកាន់ home page  នៅពេលដែល user 
    // បាន log in ហើយ តែគាត់មិនទាន់បាន  log out
    useEffect(() => {

        if (islogin) {

            if (isStudent) {
                history.push("/studentdashdoard") //move to student home page
            } else {
                history.push("/teacherhome") //move to teacher home 
            }
        }
        else {
            history.push("/") //move to login page 
        }

    }, [islogin])


    return (

        <div>
        </div>
    )
}