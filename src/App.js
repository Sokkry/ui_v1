
// link style
import './App.css';
import './style/firstPage.css';
import './style/studentLogin.css';
import './style/studentRegister.css';
import './style/studentDashboard.css';
import './style/studentHistory.css';
import './style/studentProfile.css';
import './style/studentForgetpwd.css';
import './style/studentAbout.css';
import './style/stExamPaper.css';

import './style/teacherHome.css';

import 'bootstrap/dist/css/bootstrap.min.css';
import FirstPage from './components/FirstPage';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import StudentLogin from './pages/students/StudentLogin';
import TeacherLogin from './pages/teachers/TeacherLogin';
import SetNewPassword from './pages/students/SetNewPassword';
import StudentRegister from './pages/students/StudentRegister';
import StudentDashboard from './pages/students/StudentDashboard';
import StudentHistory from './pages/students/StudentHistory';
import StudentAbout from './pages/students/StudentAbout';
import StudentProfille from './pages/students/StudentProfille';
import StudentForgetPwd from './pages/students/StudentForgetPwd';
import StudentEditProfle from './pages/students/StudentEditProfle';
import TeacherForgetPwd from './pages/teachers/TeacherForgetPwd';
import TeacherSetNewPwd from './pages/teachers/TeacherSetNewPwd';
import TeacherHome from './pages/teachers/TeacherHome';


import { useState,useEffect } from 'react';
import Checking from './Checking';


import Test from './pages/students/Test';
import StExamPaper from './pages/students/StExamPaper';

// import Plus from './icons/plus.svg'




function App() {

  const [islogin, setIsLogin] = useState(false) //use this state for verify that we used to login or not
  const [isStudent, setIsStudent] = useState(true) //use this state for verify that we are a students or not, if not are a teacher


  useEffect(() => {
    let login = JSON.parse(localStorage.getItem("islogin"))
    let stud = JSON.parse(localStorage.getItem("isStudent"))

    setIsLogin(login)
    setIsStudent(stud)
  }, [islogin])


  //this event use for let the any component page(studentsHome,teacherHome,studentsLogin,..) 
  //can verify isLogin and isStudents State
  const setEvent = (login, stud) => {

    setIsLogin(login)
    setIsStudent(stud)

  }


  return (
    <>
      <BrowserRouter>
        <Route exact path="/" component={FirstPage} />
        <Checking islogin={islogin} isStudent={isStudent}  />
        <Switch>

          {/* =============== Student */}
          <Route path="/setnewpassword" component={SetNewPassword} />
          <Route path="/studentregister" component={StudentRegister} />

          <Route path="/sudentlogin" render={() => <StudentLogin setEvent={setEvent} />} />
          <Route path="/studentdashdoard" render={() => <StudentDashboard setIsLogin={setIsLogin} />} />
          <Route path="/studenthistory" render={() => <StudentHistory setIsLogin={setIsLogin} />} />
          <Route path="/studentabout" render={() => <StudentAbout setIsLogin={setIsLogin} />} />
          <Route path="/studentprofile" render={() => <StudentProfille setIsLogin={setIsLogin} />} />
          <Route path="/studenteditprofile" render={() => <StudentEditProfle setIsLogin={setIsLogin} />} />
          <Route path="/stexampaper" component={StExamPaper} />

{/* 
          <Route path="/studentabout" component={StudentAbout} />
          <Route path="/studentprofile" component={StudentProfille} />
          <Route path="/studenteditprofile" component={StudentEditProfle} /> */}

          {/* <Route path="/studentforgetpwd" component={StudentForgetPwd} /> */}


          {/* ======================= Teacher */}

          <Route path="/teacherlogin" render={() => <TeacherLogin setEvent={setEvent} />} />
          <Route path="/teacherhome" render={() => <TeacherHome setIsLogin={setIsLogin} />} />

          <Route path="/teacherforgetpwd" component={TeacherForgetPwd} />
          <Route path="/teachersetnewpwd" component={TeacherSetNewPwd} />



          <Route path="/test" component={Test} />
        </Switch>
      </BrowserRouter>

    </>
  );
}

export default App;
