import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Form } from 'react-bootstrap';
import Modal from 'react-modal';

// =========== icon
import { RiFolderHistoryLine } from 'react-icons/ri';
import { CgProfile } from "react-icons/cg";
import { AiOutlineHome } from "react-icons/ai";
import { CgCopy } from "react-icons/cg";
import { IoLogOut } from "react-icons/io5";
import { BsFillPersonFill } from "react-icons/bs";



export default function StudentProfille({ setIsLogin }) {
    const [modalIsopen, setmodalIsopen] = useState(false)

    const [name, setName] = useState('')
    const [email, setEmail] = useState('')

    const homeEvent = () => {
        setIsLogin(false)
        localStorage.setItem("islogin", false) //store isLogin value on local storage
    }


    // ============ get data from local storage to keep in useState
    useEffect(() => {

        let stud = JSON.parse(localStorage.getItem("student"))

        setName(stud.fullName)
        setEmail(stud.email)

    }, [])

    return (
        <Container fluid className=" px-0">

            <div className="student_body_dashboard">
                <nav className="student_navbar">
                    <div className="student_nav_logo">
                        <Link to="/studentdashdoard"><img src="../images/logo2.jpg" width="50px" alt="logo" /></Link>
                    </div>
                    <ul>
                        <li className="">
                            <Link to="/studentdashdoard">
                                <div className="st_icon_home ">
                                    <label className="Hello m-0">  <AiOutlineHome />   </label>
                                </div>
                                <label htmlFor="">Home</label>
                            </Link>
                        </li>
                        <li>
                            <Link to="/studenthistory">
                                <div className="st_icon_home">
                                    <label className="Hello m-0">  <RiFolderHistoryLine />   </label>
                                </div>
                                <label htmlFor="">History</label>
                            </Link>
                        </li>
                        <li>
                            <Link to="/studentabout">
                                <div className="st_icon_home">
                                    <label className="Hello m-0">  <CgCopy />   </label>
                                </div>
                                <label htmlFor="">About</label>
                            </Link>
                        </li>
                        <li>
                            <Link to="/studentprofile" className="st_nav_active" >
                                <div className="st_icon_home">
                                    <label className="Hello m-0">  <CgProfile />   </label>
                                </div>
                                <label htmlFor="">Profile</label>
                            </Link>
                        </li>

                    </ul>
                </nav>
                <Row className="mx-0">
                    <Col>
                        <Row className="mt-4 ">
                            <Col xl="12">
                                <div className="student_profile">
                                    <div className="student_btn_join_exam">
                                        <Link to="/stexampaper">
                                            <button> + Join exam</button>
                                        </Link>
                                    </div>
                                    <div className="student_in_pro">
                                        <div class="dropdown">
                                            <input type="checkbox" name="" id="checkbox_toggle" />
                                            <label for="checkbox_toggle">
                                                <img src="./images/profile timeng.jpg" width="100%" alt="Profile" />
                                            </label>
                                            <ul>
                                                <li>
                                                    <Link to="/studentprofile">
                                                        <label className="icon"><BsFillPersonFill /></label>
                                                        Profile
                                                    </Link>
                                                </li>
                                                <li onClick={() => homeEvent()}>
                                                    <a >
                                                        <label htmlFor="" className="icon"><IoLogOut /> </label>
                                                        Logout
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <div className="student_home_dashboard">
                                    <Row>
                                        <Col lg="8">
                                            <div className="student_profile_detail">
                                                <h2 className="mb-4 ">Profile</h2>
                                                <div className="box_have_exam pt-5">
                                                    <Link to="/studenteditprofile">
                                                        <button className="btn_edit">Edit info</button>
                                                    </Link>

                                                    <Row className="mt-4">
                                                        <Col md='4' >
                                                            <div className="profile_img">
                                                                <img src="./images/profile timeng.jpg" width="100%" alt="Profile" />
                                                            </div>
                                                        </Col>
                                                        <Col md='8'>
                                                            <div className="stu_profile_detail_news">
                                                                <table>
                                                                    <tr>
                                                                        <td> Full Name</td>
                                                                        <td> Lun Timeng</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Student ID</td>
                                                                        <td> 0054403453</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Email </td>
                                                                        <td> luntimeng@gmail.com  </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Gender  </td>
                                                                        <td> Male </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Class  </td>
                                                                        <td> Phnome Phenh </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Generation </td>
                                                                        <td> 9th </td>
                                                                    </tr>

                                                                </table>
                                                                <button className="btn_save_change">Save Chage</button>
                                                                <button className="btn_change_pwd" onClick={() => setmodalIsopen(true)}>Change Password</button>

                                                                {/* ============== Modal change password */}
                                                                <Modal
                                                                    isOpen={modalIsopen}
                                                                    style={
                                                                        {
                                                                            // overlay: {
                                                                            //     backgroundColor: "gray",
                                                                            //     opacity: 0.1
                                                                            // },
                                                                            content: {
                                                                                backgroundColor: "#E5E5E5;",
                                                                                color: "#0F7C9D",
                                                                                width: 480,
                                                                                height: 280,
                                                                                margin: "auto",
                                                                                marginTop: 0,
                                                                                padding: 40,
                                                                                paddingTop: 10,
                                                                                paddingBottom: 10,
                                                                                borderRadius: 10,
                                                                                boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)"
                                                                            }

                                                                        }
                                                                    }
                                                                >
                                                                    <Form>
                                                                        <h3 className="text-center p-3">Change Password</h3>
                                                                        <Form.Group className="mb-3 textbox_st_login" controlId="formBasicEmail">
                                                                            <Form.Control type="password" placeholder="Input Current Password" />
                                                                        </Form.Group>

                                                                        <Form.Group className="mb-3 textbox_st_login" controlId="formBasicPassword">
                                                                            <Form.Control type="password" placeholder="Input New Password" />
                                                                        </Form.Group>

                                                                        <button className="btn_change__new_pwd" type="submit">
                                                                            Submit
                                                                        </button>
                                                                        {/* <Link to="/studentprofile"> */}
                                                                        <button className="btn_cancel_change_pwd"
                                                                            onClick={() => setmodalIsopen(true)}
                                                                        >
                                                                            Cancel
                                                                        </button>
                                                                        {/* </Link> */}
                                                                    </Form>
                                                                </Modal>
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                </div>
                                            </div>
                                        </Col>
                                        <Col lg="4">
                                            <div className="not">
                                                <h1>Not have content</h1>



                                            </div>
                                        </Col>
                                    </Row>
                                </div>

                            </Col>
                        </Row>
                    </Col>
                </Row>

            </div>
        </Container>
    )
}
