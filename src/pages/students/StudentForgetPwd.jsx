import React from 'react';
import Header from '../../components/Header';
import { Container, Row, Col, Form } from 'react-bootstrap';
// import { Link } from 'react-router-dom';

export default function StudentForgetPwd() {

    function OnchangePwd(){
        alert("Please check your email new password!");
    }

    return (
        <Container>
            <div className="login_container">
                <Header />
                <Row className="mt-4">
                    <Col>
                        <div className="student_forget_pwd p-5">
                            <h3 className="text-center">Reset Your Password</h3>
                            <p className="text-center">Please input your email address</p>
                            <Form>
                                <Form.Group className="mb-3 " controlId="formBasicEmail">
                                    <Form.Control type="email" placeholder="Enter Your Email" />
                                    {/* <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text> */}
                                </Form.Group>

                                <div className="btn_forget">
                                        <button 
                                            className="btn_student_submit_email" 
                                            type="submit"
                                            onClick={OnchangePwd}
                                        >Submit
                                        </button>


                                    <button className="btn_student_cancel_email" type="submit">
                                        Cancel
                                    </button>
                                </div>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </div>
        </Container>
    )
}
