import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';

// =========== icon
import { RiFolderHistoryLine } from 'react-icons/ri';
import { CgProfile } from "react-icons/cg";
import { AiOutlineHome } from "react-icons/ai";
import { CgCopy } from "react-icons/cg";
import { ImLocation } from "react-icons/im";
import { MdEmail } from "react-icons/md";
// import {  } from "react-icons/io5";
import { IoCallSharp, IoLogOut } from "react-icons/io5";
import { BsFillPersonFill } from "react-icons/bs";

export default function StudentAbout({ setIsLogin }) {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')

    const homeEvent = () => {
        setIsLogin(false)
        localStorage.setItem("islogin", false) //store isLogin value on local storage

    }


    // ============ get data from local storage to keep in useState
    useEffect(() => {

        let stud = JSON.parse(localStorage.getItem("student"))

        setName(stud.fullName)
        setEmail(stud.email)

    }, [])
    return (
        <Container fluid className=" px-0">
            <div className="student_body_dashboard">
                <nav className="student_navbar">
                    <div className="student_nav_logo">
                        <Link to="/studentdashdoard"><img src="../images/logo2.jpg" width="50px" alt="logo" /></Link>
                    </div>
                    <ul>
                        <li className="">
                            <Link to="/studentdashdoard">
                                <div className="st_icon_home ">
                                    <label className="Hello m-0">  <AiOutlineHome />   </label>
                                </div>
                                <label htmlFor="">Home</label>
                            </Link>
                        </li>
                        <li>
                            <Link to="/studenthistory">
                                <div className="st_icon_home">
                                    <label className="Hello m-0">  <RiFolderHistoryLine />   </label>
                                </div>
                                <label htmlFor="">History</label>
                            </Link>
                        </li>
                        <li>
                            <Link to="/studentabout" className="st_nav_active">
                                <div className="st_icon_home">
                                    <label className="Hello m-0">  <CgCopy />   </label>
                                </div>
                                <label htmlFor="">About</label>
                            </Link>
                        </li>
                        <li>
                            <Link to="/studentprofile" >


                                <div className="st_icon_home">
                                    <label className="Hello m-0">  <CgProfile />   </label>
                                </div>
                                <label htmlFor="">Profile</label>
                            </Link>
                        </li>

                    </ul>
                </nav>
                <Row className="mx-0">
                    <Col>
                        <Row className="mt-4 ">
                            <Col xl="12">
                                <div className="student_profile">
                                    <div className="student_btn_join_exam">
                                        <Link to="/stexampaper">
                                            <button> + Join exam</button>
                                        </Link>
                                    </div>
                                    <div className="student_in_pro">
                                        <div class="dropdown">
                                            <input type="checkbox" name="" id="checkbox_toggle" />
                                            <label for="checkbox_toggle">
                                                <img src="./images/profile timeng.jpg" width="100%" alt="Profile" />
                                            </label>
                                            <ul>
                                                <li>
                                                    <Link to="/studentprofile">
                                                        <label className="icon"><BsFillPersonFill /></label>
                                                        Profile
                                                    </Link>
                                                </li>
                                                <li onClick={() => homeEvent()}>
                                                    <a >
                                                        <label htmlFor="" className="icon"><IoLogOut /> </label>
                                                        Logout
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <div className="student_home_dashboard">
                                    <Row>
                                        <Col lg="9" md="12">
                                            <div className="student_home_dashboard_in">
                                                <h2 className="pb-2">About HRD Exam</h2>
                                                <div className="box_have_exam mb-5 mt-4">
                                                    <Row>
                                                        <Col lg="3" md="3">
                                                            <div className="st_about_logo_school">
                                                                <img src="./images/logo shcool.png" width="100%" alt="School logo" />
                                                            </div>
                                                        </Col>
                                                        <Col lg="9" md="9" >
                                                            <h6 className="mt-2">Korea Software HRD Center</h6>
                                                            <p>
                                                                Korea Software HRD Center is an academy training center for training software professionals
                                                                in cooperation with Korea International Cooperation Agency(KOICA) and Webcash in April,
                                                                2013 in Phnom Penh, Cambodia.
                                                                From 2020, Korea Software HRD Center has been become Global NGO, main sponsored by
                                                                Webcash Group, to continue mission to train SW experts, to improve SW technologies and
                                                                ICT Development in Cambodia.
                                                            </p>
                                                        </Col>
                                                    </Row>
                                                </div>

                                                <div className="box_have_exam mb-4">
                                                    <Row>

                                                        <Col lg="9" md="9">
                                                            <h6 className="mt-2">HRD Exam</h6>
                                                            <p>
                                                                HRD Exam is conducting a test online to measure the knowledge of the participants on
                                                                a given topic for KOREA SOFTWARE HRD CENTER's students. In the olden days, everybody
                                                                had to gather in a classroom at the same time to take an exam.
                                                                With HRD Exam students can do the exam online, with their own device, regardless of
                                                                where they live. You only need a browser and an internet connection.
                                                            </p>
                                                        </Col>
                                                        <Col lg="3" md="3">
                                                            <div className="st_about_logo_school">
                                                                <img src="./images/logo2.jpg" width="100%" alt="School logo" />
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                </div>

                                                <h2 className="mt-5">Contact Info</h2>
                                                <Row>
                                                    <Col lg="8" xl="9">
                                                        <div className="box_have_exam mt-3">
                                                            <Row>
                                                                <Col lg="1">
                                                                    <div className="st_contact_icon">
                                                                        <ImLocation />
                                                                    </div>
                                                                </Col>
                                                                <Col lg="11">
                                                                    <p>
                                                                        Address: #12, St 323, Sangkat Boeung Kak II, KhanToul Kork, Phnom Penh, Cambodia.
                                                                    </p>

                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col lg="1">
                                                                    <div className="st_contact_icon">
                                                                        <IoCallSharp />
                                                                    </div>
                                                                </Col>
                                                                <Col lg="11">
                                                                    <p>
                                                                        <a href="tel:+85512998919">012 998 919 (Khmer)</a> <br />
                                                                        <a href="tel:+8212998919">082 998 919 (Korean)</a> <br />
                                                                    </p>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col lg="1">
                                                                    <div className="st_contact_icon">
                                                                        <MdEmail />
                                                                    </div>
                                                                </Col>
                                                                <Col lg="11">
                                                                    {/* <a href="mailto:hrdexam@gmail.com">hrdexam@gmail.com</a> <br /> */}
                                                                    <a href="mailto:info.kshrd@gmail.com">info.kshrd@gmail.com</a> <br />
                                                                    <a href="mailto:phirum.gm@gmail.com">phirum.gm@gmail.com</a>
                                                                </Col>
                                                            </Row>
                                                        </div>
                                                    </Col>
                                                </Row>

                                            </div>
                                        </Col>
                                        {/* <Col lg="4">
                                            <div className="not">
                                                <h1>Not have content</h1>
                                            </div>
                                        </Col> */}
                                    </Row>
                                </div>

                            </Col>
                        </Row>
                    </Col>
                </Row>

            </div>
        </Container>
    )
}
