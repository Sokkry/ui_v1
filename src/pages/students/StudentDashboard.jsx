import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Dropdown } from 'react-bootstrap';

// =========== icon
import { RiFolderHistoryLine } from 'react-icons/ri';
import { CgProfile } from "react-icons/cg";
import { AiOutlineHome } from "react-icons/ai";
import { CgCopy } from "react-icons/cg";
// import StudentProHead from './StudentProHead';
import { IoLogOut } from "react-icons/io5";
import { BsFillPersonFill } from "react-icons/bs";


export default function StudentDashboard({ setIsLogin }) {

    const [name, setName] = useState('')
    const [email, setEmail] = useState('')

    const homeEvent = () => {
        setIsLogin(false)
        localStorage.setItem("islogin", false) //store isLogin value on local storage
    }


    // ============ get data from local storage to keep in useState
    useEffect(() => {

        let stud = JSON.parse(localStorage.getItem("student"))

        setName(stud.fullName)
        setEmail(stud.email)

    }, [])

    return (

        <Container fluid className=" px-0">
            <div className="student_body_dashboard">
                <nav className="student_navbar">
                    <div className="student_nav_logo">
                        <Link to="/studentdashdoard"><img src="../images/logo2.jpg" width="50px" alt="logo" /></Link>
                    </div>
                    <ul>
                        <li className="">
                            <Link to="/studentdashdoard" className="st_nav_active">
                                <div className="st_icon_home ">
                                    <label className="Hello m-0">  <AiOutlineHome />   </label>
                                </div>
                                <label htmlFor="">Home</label>
                            </Link>
                        </li>
                        <li>
                            <Link to="/studenthistory">
                                <div className="st_icon_home">
                                    <label className="Hello m-0">  <RiFolderHistoryLine />   </label>
                                </div>
                                <label htmlFor="">History</label>
                            </Link>
                        </li>
                        <li>
                            <Link to="/studentabout">
                                <div className="st_icon_home">
                                    <label className="Hello m-0">  <CgCopy />   </label>
                                </div>
                                <label htmlFor="">About</label>
                            </Link>
                        </li>
                        <li>
                            <Link to="/studentprofile" >
                                <div className="st_icon_home">
                                    <label className="Hello m-0">  <CgProfile />   </label>
                                </div>
                                <label htmlFor="">Profile</label>
                            </Link>
                        </li>

                    </ul>
                </nav>
                <Row className="mx-0">
                    <Col>
                        <Row className="mt-3 ">
                            <Col md="12">
                                {/* <StudentProHead /> */}
                                <div className="student_profile">
                                    <div className="student_btn_join_exam">
                                        <Link to="/stexampaper">
                                            <button> + Join exam</button>
                                        </Link>
                                    </div>
                                    {/*  */}
                                    <div className="student_in_pro  ">
                                        <div class="dropdown">
                                            <input type="checkbox" name="" id="checkbox_toggle" />
                                            <label for="checkbox_toggle">
                                                <img src="./images/profile timeng.jpg" width="100%" alt="Profile" />
                                            </label>
                                            <ul>
                                                <li>
                                                    <Link to="/studentprofile">
                                                        <label className="icon"><BsFillPersonFill /></label>
                                                        Profile
                                                    </Link>
                                                </li>
                                                <li onClick={() => homeEvent()}>
                                                    <a >
                                                        <label htmlFor="" className="icon"><IoLogOut /> </label>
                                                        Logout
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>

                            </Col>
                        </Row>
                        <Row >
                            <Col>
                                <div className="student_home_dashboard">
                                    <Row>
                                        <Col lg="8">
                                            <div className="student_home_dashboard_in">
                                                <h2 className="mb-4">
                                                    Hello {" "}
                                                    {name}
                                                </h2>
                                                <p>09 July 2021</p>
                                                <div className="box_have_exam">
                                                    <p className="start_exam_at">Start at: 9h 45mn</p>
                                                    <h5 className="mt-3 mb-3">You have an exam from Tong Dalen!</h5>
                                                    <p className="my-1" >Title: Korean Monthly Quiz June</p>
                                                    <p className="my-1">Sobject: Korean</p>
                                                    <p className="my-1">Duration: 60mn</p>
                                                    <Link to="/stexampaper">
                                                        <button className="btn_join_exam_in">Join exam</button>
                                                    </Link>
                                                </div>

                                                <h2 className="mb-4 mt-5">Exam History</h2>
                                                <div className="box_have_exam">
                                                    <Row>
                                                        <Col>
                                                            <p>Result</p>
                                                        </Col>
                                                        <Col>
                                                            <p className="text-right">
                                                                <Link>See All</Link>
                                                            </p>
                                                        </Col>
                                                    </Row>
                                                    <table>
                                                        <tr>
                                                            <th> NO</th>
                                                            <th> Title</th>
                                                            <th> Subject</th>
                                                            <th> Exam date</th>
                                                            <th> Duratons</th>
                                                            <th> Score</th>
                                                        </tr>
                                                        <tr>
                                                            <td>1</td>
                                                            <td> Java Monthly Exam June </td>
                                                            <td>Java </td>
                                                            <td> 09/June/2021</td>
                                                            <td>60mn </td>
                                                            <td> 40/60</td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td> Spring Monthly Exam June </td>
                                                            <td>Spring </td>
                                                            <td> 09/June/2021</td>
                                                            <td>60mn </td>
                                                            <td> 50/60</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </Col>
                                        <Col lg="4">
                                            <div className="not">
                                                <h1>Not have content </h1>
                                                <div id="caleandar"> </div>
                                                <script type="text/javascript" src="./test/caleandar.js"></script>
                                                <script type="text/javascript" src="./test/demo.js"></script>

                                            </div>
                                        </Col>
                                    </Row>
                                </div>

                            </Col>
                        </Row>
                    </Col>
                </Row>


            </div>
        </Container>
    )
}
