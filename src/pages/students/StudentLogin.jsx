import React, { useState, useEffect } from 'react';
import Header from '../../components/Header';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Form, Button } from 'react-bootstrap'
import { studentsLogin } from '../../util/services/studentsLogin.service';
import Swal from 'sweetalert2'

export default function StudentLogin({ setEvent }) {

    const [id, setID] = useState('')
    const [password, setPassword] = useState('')

    const [validId, setValidID] = useState(false)
    const [messageId, setMessageID] = useState('invalid Id')

    const [validPassword, setValidPassword] = useState(false)
    const [messagePassword, setMessagePassword] = useState('invalid password')

    const [btnDisable, setBtnDisable] = useState(true)


    // ================= Student validation id 
    useEffect(() => {
        let pattern = /^(?=[0-9])[0-9]{6}$/g
        let result = pattern.test(id)

        if (result) {
            setValidID(true)
            setMessageID('')
        } else {
            setValidID(false)
            setMessageID('Id must be number ( at least 6 number)')
        }
    }, [id])


    // ============ students validation password
    useEffect(() => {

        let patternPass = /^.{4,16}$/g
        let ressultPass = patternPass.test(password)

        if (ressultPass) {
            setValidPassword(true)
            setMessagePassword('')
        } else {
            setValidPassword(false)
            setMessagePassword('invalid Password (at least 8 character)')
        }

    }, [password])


    // ====================== btn Disable
    useEffect(() => {

        console.log("worked Id..", validId, " worked ", validPassword);
        if (validPassword && validId) {
            setBtnDisable(false)
        } else {
            setBtnDisable(true)
        }

    }, [validPassword && validId])


    useEffect(() => {
        setMessageID('')
        setMessagePassword('')
    }, [])


    //it move to student home page
    const homeEvent = async (e) => {

        e.preventDefault();

        const login = {//students object for login
            id,
            password
        }


        var obj = await studentsLogin(login)
        // console.log("stu", obj.payload)

        //sweet alert message
        if (obj !== null) {
            Swal.fire({
                title: obj.message,
                confirmButtonText: `Ok`,
            }).then((result) => {

                if (result.isConfirmed) {
                    setEvent(true, true)
                    localStorage.setItem("islogin", true)
                    localStorage.setItem("isStudent", true)
                } else {
                    setEvent(true, true)
                    localStorage.setItem("islogin", true)
                    localStorage.setItem("isStudent", true)
                }

            })
        }

    }


    return (
        <Container >
            <div className="login_container">

                <Header />
                <Row className="mt-4">
                    <Col>
                        <div className="student_login">
                            <h3 className="text-center mb-4">Login</h3>
                            <Form>
                                <Form.Group className="mb-3 textbox_st_login" controlId="formBasicEmail">
                                    <Form.Control type="text" placeholder="Student ID"
                                        onChange={(e) => setID(e.target.value)}
                                    />
                                    <Form.Text className="text-muted">
                                        <b><p style={{ color: validId ? 'green' : 'red' }}>{messageId}</p></b>
                                    </Form.Text>
                                </Form.Group>

                                <Form.Group className="mb-3 textbox_st_login" controlId="formBasicPassword">
                                    <Form.Control type="password" placeholder="Password"
                                        onChange={(e) => setPassword(e.target.value)}
                                    />
                                    <Form.Text className="text-muted" >
                                        <b><p style={{ color: validPassword ? 'green' : 'red' }}>{messagePassword}</p></b>
                                    </Form.Text>
                                </Form.Group>
                                <Link to="/studentdashdoard">
                                    <button
                                        className="btn_student_login mb-3"
                                        type="submit"
                                        disabled={btnDisable}
                                        onClick={homeEvent}
                                    >
                                        Login
                                    </button>
                                </Link>


                                <Form.Label >
                                    Don't have an account yet?  <Link to="/studentregister">Request now</Link>
                                </Form.Label>



                                <Form.Group className="text-center mt-2" controlId="formBasicPassword">
                                    <Form.Label className="text-center" >
                                        <Link to="/studentforgetpwd"  >Forget Password?</Link>
                                    </Form.Label>
                                </Form.Group>
                            </Form>
                        </div>
                    </Col>
                </Row>

            </div>
        </Container>
    )
}
