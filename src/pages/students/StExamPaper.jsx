import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'

export default function StExamPaper() {
    return (
        <Container>
            <div className="header_exam_paper">
                <div className="header_exam_logo">
                    <Row>
                        <Col lg="6" md="6" xs="6">
                            <div className="first_page_logo">
                                <img src="./images/logo.png" width="142px" alt="" />
                            </div>
                        </Col>
                        <Col lg="6" md="6" xs="6" >
                            <div className="first_page_sch_logo">
                                <img src="./images/logo shcool.png" width="50px" alt="" />
                            </div>
                        </Col>
                    </Row>
                </div>

                <div className="header_title_exam_paper">
                    <Row>
                        <Col lg="3" md="3" sm="3">
                            <p className="mt-1">50 mn 15s</p>
                        </Col>
                        <Col lg="6" md="6" sm="6" className="text-center">
                            <h4>KOREA SOFTWARE HRD CENTER</h4>
                            <p className="my-0 text-dark">Web Monthly Test June</p>
                            <p className="my-0 text-dark">Subject : Web</p>
                            <p className="my-0 text-dark">Duration : 60 mn</p>
                        </Col>
                        <Col lg="3" md="3" sm="3">
                            <button className="btn_exam_submit">Submit</button>
                        </Col>
                    </Row>
                </div>
            </div>
            <div className="content_exam_paper">
                <Row>
                    <Col lg="12">
                        <div className="exam_section">
                            <h5 className="section_title">Section l : Fill the word which best fit in each gap.</h5>
                            <p className="my-0">
                                1. React use <input type="text" name="" id="" /> for templating instead of regular JavaScript.
                            </p>
                            <p className="my-0">
                                2. We can create components with JavaScript  <input type="text" name="" id="" /> and  ES6 <input type="text" name="" id="" /> .
                            </p>
                            <p className="my-0">
                                3. <input type="text" name="" id="" /> gives a way to pass data from one component to other components.
                            </p>
                            <p className="my-0">
                                3. <input type="text" name="" id="" /> gives a way to pass data from one component to other components.
                            </p>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col lg="12">
                        <div className="exam_section">
                            s
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col lg="12">
                        <div className="exam_section">
                            s
                        </div>
                    </Col>
                </Row>
            </div>
        </Container>
    )
}
