import React from 'react';
import Header from '../../components/Header';
import { Container, Row, Col, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function SetNewPassword() {
    return (
        <Container>
            <div className="login_container">
            <Header />
            <Row className="mt-4">
                <Col>
                    <div className="student_login">
                        <h3 className="text-center mb-4">New Password</h3>
                        <Form>
                            <Form.Group className="mb-3 textbox_st_login" controlId="formBasicEmail">
                                <Form.Control type="password" placeholder="Enter New Password" />
                                {/* <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text> */}
                            </Form.Group>

                            <Form.Group className="mb-3 textbox_st_login" controlId="formBasicPassword">
                                <Form.Control type="password" placeholder="Confirm Password" />
                            </Form.Group>

                            <Link to="/sudentlogin">
                                <button className="btn_student_login mt-1" type="submit">
                                    Reset Password
                                </button>
                            </Link>

                        </Form>
                    </div>
                </Col>
            </Row>
            </div>
        </Container>
    )
}
