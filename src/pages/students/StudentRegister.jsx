import React from 'react';
import Header from '../../components/Header';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Form } from 'react-bootstrap';

export default function StudentRegister() {
    return (
        <Container>
            <div className="login_container">
                <Header />
                <Row className="mt-5">
                    <Col sm="12" lg="9">
                        <Form className="mx-4">
                            <Row>
                                <Col >
                                    <h3 className="mb-4">Request</h3>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs="12" md="6">
                                    <Form.Group className="mb-3 textbox_st_login" controlId="formBasicEmail">
                                        <Form.Control type="text" placeholder="Full Name" />
                                        {/* <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text> */}
                                    </Form.Group>
                                </Col>
                                <Col xs="12" md="6">
                                    <Form.Group className="mb-3 textbox_st_login" controlId="formBasicEmail">
                                        <Form.Control type="text" placeholder="Student ID" />
                                        {/* <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text> */}
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs="12" md="6">
                                    <Form.Group className="mb-3 textbox_st_login" controlId="formBasicEmail">
                                        <Form.Control type="text" placeholder="Email" />
                                        {/* <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text> */}
                                    </Form.Group>
                                </Col>
                                <Col xs="12" md="6">
                                    <div className="st_register_gender">
                                        <select name="" id="" >
                                            <option >Select Class</option>
                                            <option value="Male" >Phnome Penh</option>
                                            <option value="Female" >Battombong</option>
                                            <option value="Female" >Seam Reap</option>
                                        </select>
                                    </div>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs="12" md="6">
                                    <Form.Group className="mb-3 textbox_st_login" controlId="formBasicEmail">
                                        <Form.Control type="password" placeholder="Password" />
                                        {/* <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text> */}
                                    </Form.Group>
                                </Col>
                                <Col xs="12" md="6">
                                    <Form.Group className="mb-3 textbox_st_login" controlId="formBasicEmail">
                                        <Form.Control type="password" placeholder="Confirm Pasword" />
                                        {/* <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text> */}
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row>
                                <Col md="6" xs="12">
                                    <div className="st_register_gender">
                                        <select name="" id="" >
                                            <option >Select Gender</option>
                                            <option value="Male" >Male</option>
                                            <option value="Female" >Female</option>
                                        </select>
                                    </div>

                                </Col>
                            </Row>

                            <Row>
                                <Col md="6" xs="12">
                                    <button className="btn_student_login mb-3" type="submit">
                                        Request Account
                                    </button>
                                </Col>
                            </Row>

                            <Form.Label >
                                Already have an account yet?  <Link to="/sudentlogin">Login</Link>
                            </Form.Label>
                        </Form>
                    </Col>
                    <Col ms="12" lg="3">
                        <div className="student_register_profilee">
                            <h3 className="mb-4">Profile Image</h3>
                            <div className=" student_register_profile_img">
                                <img src="./images/profile.png" alt="Profile" width="100%" />
                                <p className="text-center pt-2">No Image</p>
                            </div>
                        </div>


                    </Col>
                </Row>
            </div>
        </Container>
    )
}
