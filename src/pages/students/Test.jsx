import React from 'react';
import { useState } from 'react';
import { Button, Form, } from 'react-bootstrap'
import Modal from 'react-modal'

export default function Test() {
    const [modalIsopen, setmodalIsopen] = useState(false)
    return (
        <div >
            <Button onClick={() => setmodalIsopen(true)}>Click Me </Button>
            <div className="hh">
                <Modal
                    isOpen={modalIsopen}
                    style={
                        {
                            // overlay: {
                            //     backgroundColor: "gray",
                            //     opacity: 0.1
                            // },
                            content: {
                                backgroundColor: "#E5E5E5;",
                                color: "#0F7C9D",
                                width: 480,
                                height: 280,
                                margin: "auto",
                                marginTop: 0,
                                padding: 40 ,
                                paddingTop: 10,
                                paddingBottom: 10,
                                borderRadius: 10,
                                boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)"
                            }
                            
                        }
                    }
                    >

                    <Form>
                        <h3 className="text-center p-3">Change Password</h3>
                            <Form.Group className="mb-3 textbox_st_login" controlId="formBasicEmail">
                                <Form.Control type="password" placeholder="Input Current Password" />
                            </Form.Group>

                            <Form.Group className="mb-3 textbox_st_login" controlId="formBasicPassword">
                                <Form.Control type="password" placeholder="Password" />
                            </Form.Group>
                            
                            {/* <Link to="/studentdashdoard"> */}
                                <button className="" type="submit">
                                    Submit
                                </button>
                                <button className="bg-danger" type="submit">
                                    Cancel
                                </button>
                            {/* </Link> */}

                        </Form>

                </Modal>
            </div>

        </div>
    )
}
