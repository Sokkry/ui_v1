import React, { useState, useEffect } from 'react';
import Header from '../../components/Header';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Form } from 'react-bootstrap'
import Swal from 'sweetalert2'
import { teacherLogin } from '../../util/services/teachersLogin.service';

export default function TeacherLogin({ setEvent }) {

    const [username, setUsername] = useState('')

    const [password, setPassword] = useState('')

    const [validUsername, setValidUsername] = useState(false)
    const [messageUsername, setMessageUsername] = useState('')

    const [validPassword, setValidPassword] = useState(false)
    const [messagePassword, setMessagePassword] = useState('')

    const [btnDisable, setBtnDisable] = useState(true)

    // =============== teacher validation username
    useEffect(() => {
        let pattern = /^(?=[a-z])[a-z]{2,16}$/g
        let result = pattern.test(username)

        if (result) {
            setValidUsername(true)
            setMessageUsername('')
        } else {
            setValidUsername(false)
            setMessageUsername('Username must be uppercase letter ( at least 2 number)')
        }
    }, [username])

    // ================== teachers validation password
    useEffect(() => {

        let patternPass = /^.{3,16}$/g
        let ressultPass = patternPass.test(password)

        if (ressultPass) {
            setValidPassword(true)
            setMessagePassword('')
        } else {
            setValidPassword(false)
            setMessagePassword('invalid Password (at least 8 character)')
        }

    }, [password])



    // ================== btn Disable
    useEffect(() => {

        console.log("worked Id..", validUsername, " worked ", validPassword);
        if (validPassword && validUsername) {
            setBtnDisable(false)
        } else {
            setBtnDisable(true)
        }

    }, [validPassword && validUsername])


    useEffect(() => {
        setMessageUsername('')
        setMessagePassword('')
    }, [])

    //it move to teacher home page
    const teacherHome = async () => {

        const login = {//teachers object for login
            username,
            password
        }

        var obj = await teacherLogin(login)

        //sweet alert message
        if (obj !== null) {
            Swal.fire({
                title: obj.message,
                confirmButtonText: `Ok`,
            }).then((result) => {

                if (result.isConfirmed) {
                    setEvent(true, false)
                    localStorage.setItem("islogin", true)
                    localStorage.setItem("isStudent", false)
                } else {
                    setEvent(true, false)
                    localStorage.setItem("islogin", true)
                    localStorage.setItem("isStudent", false)
                }

            })
        }

    }

    return (
        <Container>
            <div className="login_container">
                <Header />
                <Row className="mt-4">
                    <Col>
                        <div className="student_login">
                            <h3 className="text-center mb-4">Login</h3>
                            <Form>
                                <Form.Group className="mb-3 textbox_st_login" controlId="formBasicEmail">
                                    <Form.Control type="text" placeholder="username"
                                     onChange={(e) => setUsername(e.target.value)}
                                      />
                                    <Form.Text className="text-muted">
                                        <b><p style={{ color: validUsername ? 'green' : 'red' }}>{messageUsername}</p></b>
                                    </Form.Text>
                                </Form.Group>

                                <Form.Group className="mb-3 textbox_st_login" controlId="formBasicPassword">
                                    <Form.Control type="password" placeholder="Password"
                                     onChange={(e) => setPassword(e.target.value)}
                                      />
                                    <Form.Text className="text-muted" >
                                        <b><p style={{ color: validPassword ? 'green' : 'red' }}>{messagePassword}</p></b>
                                    </Form.Text>
                                </Form.Group>
                                <Row>
                                    {/* <Col>
                                        <Form.Group className="mb-3" controlId="formBasicCheckbox">
                                            <Form.Check type="checkbox" label="Check me out" />
                                        </Form.Group>
                                    </Col> */}
                                    <Col className="text-right">
                                        <Form.Label >
                                            <Link to="/teacherforgetpwd">Forget Password?</Link>
                                        </Form.Label>
                                    </Col>
                                </Row>

                                <Link to="/teacherhome">
                                    <button 
                                        className="btn_student_login mb-3" 
                                        type="submit"
                                        disabled={btnDisable}
                                        onClick={teacherHome}
                                        >
                                        Login
                                    </button>
                                </Link>

                            </Form>
                        </div>
                    </Col>
                </Row>
            </div>
        </Container>
    )
}
