import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
// import UserGraduate from '../../../public/icons/user-graduate-solid.svg'


// =========== icon
import { Fa500Px, FaYahoo, FaHome, FaUserGraduate } from "react-icons/fa";
import { CgProfile } from "react-icons/cg";
import { AiTwotoneHome } from "react-icons/ai";
import { TiGroup } from "react-icons/ti";
import { IoIosCopy} from "react-icons/io";
import { IoLogOut } from "react-icons/io5";
import { BsPersonFill,BsPersonCheckFill,BsFillPersonFill } from "react-icons/bs";

// t
import { AiOutlineHome } from "react-icons/ai";
import { CgCopy } from "react-icons/cg";
import { RiFolderHistoryLine } from 'react-icons/ri';

export default function TeacherHome({ setIsLogin }) {

    const [name, setName] = useState('')
    const [email, setEmail] = useState('')



    const homeEvent = () => {

        setIsLogin(false)

        localStorage.setItem("islogin", false) // ======== set isLogin on local storage

    }


    // ============ get data from local storage to keep in useState
    useEffect(() => {

        let teacher = JSON.parse(localStorage.getItem("teacher"))

        if (teacher !== null) {

            setName(teacher.fullName)
            setEmail(teacher.email)
        }

    }, [])

    return (
        <Container fluid className=" px-0">
            <div className="student_body_dashboard">
                <nav className="student_navbar">
                    <div className="student_nav_logo">
                        <Link to="/studentdashdoard"><img src="../images/logo2.jpg" width="50px" alt="logo" /></Link>
                    </div>
                    <ul>
                        <li className="">
                            <Link to="/studentdashdoard" className="st_nav_active">
                                <div className="st_icon_home ">
                                    <label className="Hello m-0">  <AiTwotoneHome />    </label>
                                </div>
                                <p>Home</p>
                            </Link>
                        </li>
                        <li>
                            <Link to="/studenthistory">
                                <div className="st_icon_home">
                                    <label className="Hello m-0">  <IoIosCopy />   </label>
                                </div>
                                <p>Class Work</p>
                            </Link>
                        </li>
                        <li>
                            <Link to="/studentabout">
                                <div className="st_icon_home">
                                    <label className="Hello m-0">  <FaUserGraduate />   </label>
                                </div>
                                <p>Student</p>
                            </Link>
                        </li>
                        <li>
                            <Link to="/studentprofile" >
                                <div className="st_icon_home">
                                    <label className="Hello m-0">  <BsPersonCheckFill />   </label>
                                </div>
                                <p>Request</p>
                            </Link>
                        </li>
                        <li>
                            <Link to="/studentprofile" >
                                <div className="st_icon_home">
                                    <label className="Hello m-0">  < TiGroup/>   </label>
                                </div>
                                <p>Generation</p>
                            </Link>
                        </li>
                        <li>
                            <Link to="/studentprofile" >
                                <div className="st_icon_home">
                                    <label className="Hello m-0">  < BsPersonFill/>   </label>
                                </div>
                                <p>Profile</p>
                            </Link>
                        </li>

                    </ul>
                </nav>
                
                <Row className="mx-0">
                    <Col>
                        <Row className="mt-4 ">
                            <Col xl="12">
                                <div className="student_profile">
                                    <div className="student_btn_join_exam">
                                        <button> + Create Exam </button>
                                    </div>
                                    <div className="student_in_pro">
                                    <div class="dropdown">
                                            <input type="checkbox" name="" id="checkbox_toggle" />
                                            <label for="checkbox_toggle">
                                                <img src="./images/profile timeng.jpg" width="100%" alt="Profile" />
                                            </label>
                                            <ul>
                                                <li>
                                                    <Link to="/studentprofile">
                                                        <label className="icon"><BsFillPersonFill /></label>
                                                        Profile
                                                    </Link>
                                                </li>
                                                <li onClick={() => homeEvent()}>
                                                    <a >
                                                        <label htmlFor="" className="icon"><IoLogOut /> </label>
                                                        Logout
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        {/* <img src="./images/profile teacher.jpg" width="100%" alt="Profile" /> */}
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <div className="student_home_dashboard">
                                    <Row>
                                        <Col xl="8">
                                            <div className="student_home_dashboard_in">
                                                <h2>Hello Timeng </h2>
                                                <p>09 July 2021</p>
                                                <Row>
                                                    <Col md="4">
                                                        <div className="teacher_card_total_st">
                                                            <p>Student</p>
                                                            <div className="teacher_card_total_st_img">
                                                                <img src="./images/user graduate.png" width="100%" alt="add" />
                                                            </div>
                                                            <Row>
                                                                <Col>4</Col>
                                                                <Col>4</Col>
                                                            </Row>
                                                        </div>
                                                    </Col>
                                                    <Col md="4">
                                                        <div className="teacher_card_total_class_work">
                                                            d
                                                        </div>
                                                    </Col>
                                                    <Col md="4">
                                                        <div className="teacher_card_total_st_request">
                                                            d
                                                        </div>
                                                    </Col>
                                                    <Col md="4">
                                                        <div className="teacher_card_total_st_generation">
                                                            d
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </div>
                                        </Col>
                                        <Col xl="4">
                                            <div className="not">
                                                <h1>Not have content</h1>
                                                <Fa500Px />
                                                <FaYahoo />
                                                <FaHome />

                                            </div>
                                        </Col>
                                    </Row>
                                </div>

                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        </Container>
    )
}
