import StringCrypto from "string-crypto";
import api from "../api";

const {

    encryptString,
    decryptString,

} = new StringCrypto();



//teacher login ==
export const teacherLogin = async (login) => {

    try {

        const response = await api.post("/teacher/login", login)

        if (response.data.success) {

            let teacher = response.data.payload

            //encrypt token
            let encrypToken = encryptString(teacher.token, process.env.REACT_APP_SECRET)

            teacher.token = encrypToken

            localStorage.setItem("teacher", JSON.stringify(teacher))

        }

        return response.data
        
    } catch (error) {

        console.log("handler teacher ", error)
    }

}