import StringCrypto from "string-crypto";
import api from "../api";


const {
    encryptString,
    // decryptString,

} = new StringCrypto();

//students login == 
export const studentsLogin = async (login) => {

    try {

        const response = await api.post("/student/login", login)

        if (response.data.success) {

            let student = response.data.payload

            //encrypt token
            let encrypToken = encryptString(student.token, process.env.REACT_APP_SECRET)

            student.token = encrypToken

            localStorage.setItem("student", JSON.stringify(student))

        }

        return response.data

    } catch (error) {

        console.log("hanlde students ", error)
    }
}